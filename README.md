<p align="left">
  <img width="100" height="100" src="https://avatars.githubusercontent.com/u/62357110">
</p>

# Vizality

Vizality is an @discord client modification that is based off of Powercord.

### What is Vizality?

Vizality is a client modification for the Discord desktop app, meant to give it enhanced customization of features and appearance. Client modifications are technically against Discord's Terms of Service, so use it at your own risk.

### Installation
Dependencies:
* Node.js (Latest or LTS)
* Git
* Discord Stable (Canary and PTB are not supported)

Installation (Windows):
* Clone the repository using git with this command: `git clone https://github.com/vizality/vizality`
* Run `cd vizality`
* Run `npm run inject`
* And restart your discord client **completely** through the system tray.

Installation (\*nix):
* Clone the repository using git with this command: `git clone https://github.com/vizality/vizality`
* Run `cd vizality`
* Run `node src/inject inject --no-exit-codes` **as root**
* And restart your discord client **completely** through the system tray.

### Uninstallation
Uninstallation (Windows):
* Go to the Vizality's folder using `cd` command.
* Run `npm run uninject`
* And restart your discord client **completely** through the system tray.

Uninstallation (\*nix):
* Go to the Vizality's folder using the `cd` command.
* Run `node src/inject uninject --no-exit-codes` **as root**
* And restart your discord client **completely** through the system tray.
### Support

You can reach out to us on our Discord support server [here](https://discord.gg/Fvmsfv2) if you need any support.

### Warning
Vizality is in a unfinished alpha state. Please report any bugs at our [Discord server](https://discord.gg/Fvmsfv2) or in the [issues](https://github.com/vizality/vizality/issues).
