import { Directories } from '@vizality/constants';
import { join } from 'path';

import Plugin from './Plugin';

/**
 * @todo Finish writing this.
 * Main class for Vizality builtins.
 * @extends Plugin
 * @extends Updatable
 * @extends Events
 */
export default class Builtin extends Plugin {
  constructor () {
    super();
    this.dir = Directories.BUILTINS;
    this.path = join(this.dir, this.addonId);
    this.type = 'builtin';
    this._labels = [ 'Builtin', this.constructor?.name ];
  }

  /**
   * Disables updates for builtins.
   * @returns {void}
   * @private
   */
  _update () {
    return this.log('Builtins cannot be updated individually!');
  }
}
