import { FluxDispatcher } from '@vizality/webpack';
import { ActionTypes } from '@vizality/constants';

export default {
  toggleSetting (category, setting, defaultValue) {
    FluxDispatcher.dirtyDispatch({
      type: ActionTypes.VIZALITY_TOGGLE_SETTING,
      category,
      setting,
      defaultValue
    });
  },

  updateSettings (category, settings) {
    FluxDispatcher.dirtyDispatch({
      type: ActionTypes.VIZALITY_UPDATE_SETTINGS,
      category,
      settings
    });
  },

  updateSetting (category, setting, value) {
    FluxDispatcher.dirtyDispatch({
      type: ActionTypes.VIZALITY_UPDATE_SETTING,
      category,
      setting,
      value
    });
  },

  deleteSetting (category, setting) {
    FluxDispatcher.dirtyDispatch({
      type: ActionTypes.VIZALITY_DELETE_SETTING,
      category,
      setting
    });
  }
};
