/**
 * This gets the APIs from the vizality global and then exports them here so that they
 * may be used by plugin developers in other forms if they wish, such as:
 * @example
 * ```
 * import { Keybinds } from '@vizality/api';
 * ```
 */

const { connections, settings, commands, keybinds, actions, notices, popups, routes, i18n, rpc } = vizality.api;

export {
  connections as Connections,
  settings as Settings,
  commands as Commands,
  keybinds as Ketbinds,
  actions as Actions,
  notices as Notices,
  popups as Popups,
  routes as Routes,
  i18n as I18n,
  rpc as RPC
};

export default this;
