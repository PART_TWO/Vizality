import { Builtin } from '@vizality/entities';

import * as commands from './commands';
import * as i18n from './i18n';

export default class AddonsManager extends Builtin {
  start () {
    this.injectStyles('styles/main.scss');
    vizality.api.i18n.injectAllStrings(i18n);

    commands.registerCommands('plugin');
    commands.registerCommands('theme');
  }

  stop () {
    vizality.api.commands.unregisterCommand('plugin');
    vizality.api.commands.unregisterCommand('theme');
  }
}
