import { existsSync, lstatSync } from 'fs';
import { join } from 'path';

import { toPlural, toTitleCase } from '@vizality/util/string';
import { toSentence } from '@vizality/util/array';
import { Directories } from '@vizality/constants';

export default {
  command: 'install',
  description: 'Installs an addon.',
  icon: 'CloudDownload',
  options: [
    { name: 'url', required: true },
    { name: 'id', required: true }
  ],
  async executor (args, type) {
    args = [ args ]?.flat();
    for (const addon of args) {
      await vizality.manager[toPlural(type)].install(addon);
    }
  }
};
