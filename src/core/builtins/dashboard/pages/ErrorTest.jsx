import { Section, Content, Layout } from '@vizality/components/dashboard';
import React, { memo } from 'react';

export default memo(() => {
  return (
    <Layout>
      <Content heading='Error Test'>
        <Section>
          {new Error()}
        </Section>
      </Content>
    </Layout>
  );
});
