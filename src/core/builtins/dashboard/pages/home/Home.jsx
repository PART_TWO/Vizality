import { Section } from '@vizality/components/dashboard';
import React, { memo } from 'react';

import Features from './Features';
import CTA from './CTA';

export default memo(() => {
  return (
    <>
      <Section className='vz-dashboard-home-section-cta'>
        <CTA />
      </Section>
      <Section className='vz-dashboard-home-section-features'>
        <Features />
      </Section>
    </>
  );
});
