/**
 * Applies attributes based on if the overlay is active.
 * @module Overlay
 * @memberof Builtin.Attributes.Misc
 */

export const labels = [ 'Overlay' ];

export default () => {
  const root = document.documentElement;
  if (window.__OVERLAY__) root.setAttribute('vz-overlay', '');
  return () => root.removeAttribute('vz-overlay');
};
