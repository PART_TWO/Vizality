/**
 * Applies attributes to blocked messages.
 * @module BlockedMessageGroup
 * @memberof Builtin.Attributes.Chat
 */

import { findInReactTree } from '@vizality/util/react';
import { patch, unpatch } from '@vizality/patcher';
import { getModule } from '@vizality/webpack';

export const labels = [ 'Chat', 'BlockedMessageGroup' ];

export default builtin => {
  const BlockMessages = getModule(m => m.type?.displayName === 'BlockedMessages');
  patch('vz-attributes-blocked-messages', BlockMessages, 'type', (_, res) => {
    try {
      const props = findInReactTree(res, r => r.count);
      if (!props) {
        return;
      }
      res.props['vz-blocked-message'] = '';
      res.props['vz-message-count'] = props.count;
      res.props['vz-expanded'] = Boolean(props.expanded) && '';
      res.props['vz-collapsed'] = Boolean(!props.expanded) && '';
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-blocked-messages');
};
