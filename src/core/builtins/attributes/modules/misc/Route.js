/**
 * Applies attributes based on the current route.
 * @module Route
 * @memberof Builtin.Attributes.Misc
 */

import { getModule } from '@vizality/webpack';
import { router } from '@discord/util';

export const labels = [ 'Misc', 'Route' ];

export default () => {
  const root = document.documentElement;

  /**
   * Watch for route changes and set the new route on change
   */
  const handleRouteChange = () => {
    const currentGuildId = getModule('getLastSelectedGuildId')?.getGuildId();
    const currentChannelId = getModule('getLastSelectedChannelId')?.getChannelId();
    const currentRoute = vizality.api.routes.getLocation();
    root.setAttribute('vz-route', currentRoute?.name);
    currentGuildId
      ? root.setAttribute('vz-guild-id', currentGuildId)
      : root.removeAttribute('vz-guild-id');
    currentChannelId
      ? root.setAttribute('vz-channel-id', currentChannelId)
      : root.removeAttribute('vz-channel-id');
  };

  /**
   * Trigger the route change initially to set attributes on startup.
   */
  handleRouteChange();

  /**
   * Add a route listener that will trigger handleRouteChange method on navigation.
   */
  router?.listeners?.add(handleRouteChange);

  /**
   * Remove attributes and remove listeners on unload.
   */
  return () => {
    root.removeAttribute('vz-route');
    root.removeAttribute('vz-guild-id');
    root.removeAttribute('vz-channel-id');
    router?.listeners?.delete(handleRouteChange);
  };
};
