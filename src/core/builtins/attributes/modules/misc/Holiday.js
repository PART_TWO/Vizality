/**
 * Applies attributes based on the current date.
 * @module Holiday
 * @memberof Builtin.Attributes.Misc
 */

export const labels = [ 'Misc', 'Holiday' ];

export default () => {
  const root = document.documentElement;
  const date = new Date();
  /*
   * Months start at 0, but dates start at 1... Weird.
   */
  if (date.getMonth() === 0 && date.getDate() === 1) {
    root.setAttribute('vz-holiday', 'new-years');
  }
  if (date.getMonth() === 3 && date.getDate() === 1) {
    root.setAttribute('vz-holiday', 'april-fools');
  }
  if (date.getMonth() === 9 && date.getDate() === 31) {
    root.setAttribute('vz-holiday', 'halloween');
  }
  if (date.getMonth() === 11 && date.getDate() === 25) {
    root.setAttribute('vz-holiday', 'christmas');
  }

  /**
   * Remove the attribute on unload.
   */
  return () => root.removeAttribute('vz-holiday');
};
