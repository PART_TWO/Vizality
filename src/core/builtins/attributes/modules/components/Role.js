/**
 * Applies attributes to guild member roles.
 * @module Role
 * @memberof Builtin.Attributes.Components
 */

import { patch, unpatch } from '@vizality/patcher';
import { getModule } from '@vizality/webpack';

export const labels = [ 'Components', 'Role' ];

export default async builtin => {
  const { MemberRole } = getModule('MemberRole');
  patch('vz-attributes-roles', MemberRole, 'render', ([ props ], res) => {
    try {
      if (!props?.role) return;
      res.props.children.props['vz-role-id'] = props.role.id;
      res.props.children.props['vz-role-name'] = props.role.name;
      res.props.children.props['vz-role-color-string'] = props.role.colorString;
      res.props.children.props['vz-hoisted'] = Boolean(props.role.hoist) && '';
      res.props.children.props['vz-mentionable'] = Boolean(props.role.mentionable) && '';
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-roles');
};
