/**
 * Applies attributes to empty game icons (the question mark blocks).
 * @module GameIcon
 * @memberof Builtin.Attributes.Components
 */

import { patch, unpatch } from '@vizality/patcher';
import { getModule } from '@vizality/webpack';

export const labels = [ 'Components', 'GameIcon' ];

export default builtin => {
  const GameIcon = getModule(m => m.default?.displayName === 'GameIcon');
  patch('vz-attributes-game-icon', GameIcon, 'default', ([ props ], res) => {
    try {
      res.props['vz-no-icon'] = Boolean(props?.game) && '';
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-game-icon');
};
