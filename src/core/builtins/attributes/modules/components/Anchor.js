/**
 * Patches anchor links, giving them additional or alternate functionality.
 * @module Anchor
 * @memberof Builtin.Enhancements.Components
 */

import { findInReactTree } from '@vizality/util/react';
import { patch, unpatch } from '@vizality/patcher';
import { getModule } from '@vizality/webpack';
import { Regexes } from '@vizality/constants';
import { fetchUser } from '@discord/user';

export const labels = [ 'Components', 'Anchor' ];

export default builtin => {
  const Anchor = getModule(m => m.default?.displayName === 'Anchor');
  const { anchor } = getModule('anchor', 'anchorUnderlineOnHover');
  const vizalityRegex = new RegExp(`${Regexes.DISCORD}/vizality`, 'i');
  const vizalityProtocolRegex = new RegExp('^(vizality://)', 'i');
  const userRegex = new RegExp(`${Regexes.DISCORD}/users/`, 'i');
  const discordRegex = new RegExp(`${Regexes.DISCORD}`, 'i');

  patch('vz-attributes-anchors', Anchor, 'default', (_, res) => {
    try {
      const props = findInReactTree(res, r => r.className?.includes(anchor) && r.href);
      if (!props) return;
      /**
       * Force Vizality route links open within the app.
       */
      if (vizalityRegex.test(props.href)) {
        const route = props.href?.replace(discordRegex, '');
        props.onClick = e => {
          e.preventDefault();
          vizality.api.routes.navigateTo(route);
        };
      }
      /**
       * Force Vizality protocol links open within the app.
       */
      if (vizalityProtocolRegex.test(props.href)) {
        const route = props.href?.replace(vizalityProtocolRegex, '');
        props.onClick = e => {
          e.preventDefault();
          vizality.api.routes.navigateTo(`/vizality/${route}`);
        };
      }
      /*
       * Force user links open in user profile modals within the app.
       */
      if (userRegex.test(props.href)) {
        const userId = props.href?.replace(userRegex, '');
        props.onClick = e => {
          e.preventDefault();
          if (!userId) return;
          fetchUser(userId)
            .then(() => getModule('open', 'fetchProfile')?.open(userId))
            .catch(() => vizality.api.notifications.sendToast({
              id: 'VIZALITY_USER_NOT_FOUND',
              header: 'User Not Found',
              type: 'User Not Found',
              content: 'That user was unable to be located.',
              icon: 'PersonRemove'
            }));
        };
      }
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-anchors');
};
