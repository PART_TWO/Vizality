/**
 * Applies attributes for context menus.
 * @module ContextMenu
 * @memberof Builtin.Attributes.Components.ContextMenu
 */

import { getModuleByDisplayName } from '@vizality/webpack';
import { patch, unpatch } from '@vizality/patcher';

export const labels = [ 'Components', 'ContextMenu' ];

export default builtin => {
  const ContextMenu = getModuleByDisplayName('FluxContainer(ContextMenus)');
  patch('vz-attributes-context-menu', ContextMenu?.prototype, 'render', (_, res) => {
    try {
      if (!res.props) return;
      const root = document.documentElement;
      /**
       * If a context menu is opened, add an attribute to <html>
       * If not, remove the attribute from <html> if it's currently there.
       */
      res.props.isOpen
        ? root.setAttribute('vz-context-menu-active', '')
        : root.removeAttribute('vz-context-menu-active');
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-context-menu');
};
