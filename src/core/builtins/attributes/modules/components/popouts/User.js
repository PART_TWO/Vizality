/**
 * Applies attributes to user popouts.
 * @module User
 * @memberof Builtin.Attributes.Components.Popouts
 */

import { patch, unpatch } from '@vizality/patcher';
import { findInTree } from '@vizality/util/react';
import { getModule } from '@vizality/webpack';

export const labels = [ 'Components', 'Popouts', 'User' ];

export default builtin => {
  const UserPopout = getModule(m => m.default?.displayName === 'ConnectedUserPopout');
  patch('vz-attributes-popout-user', UserPopout, 'default', (_, res) => {
    try {
      res.ref = elem => {
        if (elem && elem._reactInternalFiber) {
          const container = findInTree(elem._reactInternalFiber?.return, el => el.stateNode, { walkable: [ 'return' ] });
          container.stateNode?.children[0].setAttribute('vz-user-id', res.props.user.id);
          container.stateNode?.parentElement.setAttribute('vz-popout', 'user');
        }
      };
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-popout-user');
};
