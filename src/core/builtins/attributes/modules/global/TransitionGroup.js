/**
 * Modifies The TransitionGroup component. We are checking for and modifying
 * settings content sections in particular here, but it works for all TransitionGroups.
 * @module TransitionGroup
 * @memberof Builtin.Attributes.Global
 */

import { getModuleByDisplayName, getModule } from '@vizality/webpack';
import { findInReactTree } from '@vizality/util/react';
import { toKebabCase } from '@vizality/util/string';
import { patch, unpatch } from '@vizality/patcher';

export const labels = [ 'Global', 'TransitionGroup' ];

export default builtin => {
  const TransitionGroup = getModuleByDisplayName('TransitionGroup');
  const { contentRegion } = getModule('contentRegion');
  patch('vz-attributes-transition-group', TransitionGroup?.prototype, 'render', (_, res) => {
    try {
      if (!res.props?.className?.includes(contentRegion)) return;
      const section = findInReactTree(res, c => c.section)?.section;
      section && (res.props['vz-section'] = toKebabCase(section));
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-transition-group');
};
