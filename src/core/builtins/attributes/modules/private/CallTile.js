/**
 * 
 * @module CallTile
 * @memberof Builtin.Attributes.Private
 */

import { patch, unpatch } from '@vizality/patcher';
import { getModule } from '@vizality/webpack';

export const labels = [ 'Private', 'CallTile' ];

export default builtin => {
  const CallTile = getModule(m => m.default?.displayName === 'CallTile')?.default;
  patch('vz-attributes-private-call', CallTile, 'type', ([ props ], res) => {
    try {
      const { participant } = props;
      if (!participant) return;
      res.props['vz-self-mute'] = Boolean(participant.voiceState?.selfMute) && '';
      res.props['vz-self-deaf'] = Boolean(participant.voiceState?.selfDeaf) && '';
      res.props['vz-video'] = Boolean(participant.voiceState?.selfVideo) && '';
      res.props['vz-speaking'] = Boolean(participant.speaking) && '';
      res.props['vz-ringing'] = Boolean(participant.ringing) && '';
      res.props['vz-user-id'] = participant.id;
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-private-call');
};
