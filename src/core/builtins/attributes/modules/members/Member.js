/**
 * Applies attributes to members on the members sidebar.
 * @module Member
 * @memberof Builtin.Attributes.Misc
 */

import { getModuleByDisplayName } from '@vizality/webpack';
import { patch, unpatch } from '@vizality/patcher';

export const labels = [ 'Members', 'Member' ];

export default builtin => {
  const MemberListItem = getModuleByDisplayName('MemberListItem');
  patch('vz-attributes-members-member', MemberListItem?.prototype, 'render', function (_, res) {
    try {
      if (!res || !this?.props?.user) return;
      const { activities } = res.props?.subText?.props;
      const { user, isOwner } = this.props;
      if (!user) return;
      res.props['vz-user-id'] = user.id;
      res.props['vz-activity'] = Boolean(activities?.some(activity => activity.type !== 4)) && '';
      res.props['vz-self'] = Boolean(user.email) && '';
      res.props['vz-bot'] = Boolean(user.bot) && '';
      res.props['vz-owner'] = Boolean(isOwner) && '';
    } catch (err) {
      return builtin.error(builtin._labels.concat(labels), err);
    }
  });
  return () => unpatch('vz-attributes-members-member');
};
