import React, { memo } from 'react';

import { ContextMenu, LazyImage } from '@vizality/components';
import { toPlural } from '@vizality/util/string';
import { useForceUpdate } from '@vizality/hooks';
import { Messages } from '@vizality/i18n';

export default memo(() => {
  const forceUpdate = useForceUpdate();

  const plugins =
    vizality.manager.plugins.keys
      .sort((a, b) => a - b)
      .map(plugin => vizality.manager.plugins.get(plugin));

  const themes =
    vizality.manager.themes.keys
      .sort((a, b) => a - b)
      .map(theme => vizality.manager.themes.get(theme));

  const renderContextItem = (item, type) => {
    return (
      <ContextMenu.CheckboxItem
        vz-addon-icon={item.manifest.icon}
        id={item.addonId}
        label={item.manifest.name}
        checked={vizality.manager[toPlural(type)].isEnabled(item.addonId)}
        action={async () => {
          vizality.manager[toPlural(type)].isEnabled(item.addonId)
            ? await vizality.manager[toPlural(type)].disable(item.addonId)
            : await vizality.manager[toPlural(type)].enable(item.addonId);
          forceUpdate();
        }}
      >
        <LazyImage src={item.manifest.icon} />
      </ContextMenu.CheckboxItem>
    );
  };

  return (
    <>
      <ContextMenu.Separator />
      <ContextMenu.Item
        id='vizality'
        label='Vizality'
        action={() => vizality.api.routes.navigateTo('dashboard')}
      >
        <ContextMenu.Item
          id='settings'
          label='Settings'
          action={() => vizality.api.routes.navigateTo('settings')}
        />
        <ContextMenu.Item
          id='plugins'
          label='Plugins'
          action={() => vizality.api.routes.navigateTo('plugins')}
        >
          {plugins.length && plugins.map(plugin => renderContextItem(plugin, 'plugins'))}
        </ContextMenu.Item>
        <ContextMenu.Item
          id='themes'
          label='Themes'
          action={() => vizality.api.routes.navigateTo('themes')}
        >
          {themes.length && themes.map(theme => renderContextItem(theme, 'themes'))}
        </ContextMenu.Item>
        <ContextMenu.Item
          id='snippets'
          label='Snippets'
          disabled={true}
          action={() => vizality.api.routes.navigateTo('snippets')}
        >
        </ContextMenu.Item>
        {vizality.manager.builtins.isEnabled('quick-code') && (
          <ContextMenu.Item
            id='quick-code'
            label='Quick Code'
            action={() => vizality.api.routes.navigateTo('quick-code')}
          />
        )}
        <ContextMenu.Separator/>
        <ContextMenu.Item
          id='development'
          label='Development'
          action={() => vizality.api.routes.navigateTo('development')}
        >
          <ContextMenu.Item
            id='documentation'
            label='Documentation'
            action={() => vizality.api.routes.navigateTo('docs')}
          >
            <ContextMenu.Item
              id='getting-started'
              label='Getting Started'
              action={() => vizality.api.routes.navigateTo('docs/getting-started')}
            />
            <ContextMenu.Item
              id='plugins'
              label='Plugins'
              action={() => vizality.api.routes.navigateTo('docs/plugins')}
            />
            <ContextMenu.Item
              id='themes'
              label='Themes'
              action={() => vizality.api.routes.navigateTo('docs/themes')}
            />
            <ContextMenu.Item
              id='screenshots'
              label='Screenshots'
              action={() => vizality.api.routes.navigateTo('docs/components/screenshots')}
            />
            <ContextMenu.Item
              id='icons'
              label='Components'
              action={() => vizality.api.routes.navigateTo('docs/components/icons')}
            />
            <ContextMenu.Item
              id='markdown'
              label='Markdown'
              action={() => vizality.api.routes.navigateTo('docs/components/markdown')}
            />
            <ContextMenu.Item
              id='error-test'
              label='Error Test'
              action={() => vizality.api.routes.navigateTo('docs/components/error-test')}
            />
            <ContextMenu.Item
              id='test'
              label='Test'
              action={() => vizality.api.routes.navigateTo('docs/components/test')}
            />
          </ContextMenu.Item>
        </ContextMenu.Item>
        <ContextMenu.Separator/>
        <ContextMenu.Item
          id='updater'
          label='Updater'
          action={() => vizality.api.routes.navigateTo('updater')}
        />
        <ContextMenu.Item
          id='changelog'
          label='Changelog'
          action={() => vizality.api.routes.navigateTo('changelog')}
        />
      </ContextMenu.Item>
    </>
  );
});
