import { ToastContainer, toast as sendToast } from 'react-toastify';
import { log, warn, error } from '@vizality/util/logger';
import { joinClassNames } from '@vizality/util/dom';
import React, { memo, useEffect } from 'react';
import { Events } from '@vizality/constants';
import { Icon } from '@vizality/components';

import Toast from './Toast';

/** @private */
const _labels = [ 'Component', 'ToastContainer' ];
/* eslint-disable no-unused-vars */
const _log = (...message) => log({ labels: _labels, message });
const _warn = (...message) => warn({ labels: _labels, message });
const _error = (...message) => error({ labels: _labels, message });

export default memo(({ settings }) => {
  // const [ toastId, setToastId ] = React.useState(null);
  const _handleToastSend = toastId => {
    // setToastId(toastId);
    const toast = vizality.api.notifications.getToastById(toastId);
    sendToast(<Toast toast={toast} />, {
      toastId,
      closeButton: false,
      delay: toast.delay,
      autoClose: toast.timeout,
      progressStyle: toast.type,
      bodyClassName: 'vz-toast-body',
      progressClassName: 'vz-toast-progress-bar',
      className: joinClassNames('vz-toast', {
        'Toastify__toast--show-progress-bar': toast.progressBar || (settings.get('toastTimeout', true) && settings.get('showProgressBar', false)),
        'Toastify__toast--extra-padding': toast.buttons?.length || toast.content || toast.custom,
        'Toastify__toast--custom': toast.custom,
        'Toastify__toast--no-footer': !toast.buttons?.length,
        'Toastify__toast--no-content': !toast.content
      }),
      onClose: () => vizality.api.notifications.closeToast(toastId)
    });
  };

  useEffect(() => {
    // @todo Ask Strencher about this.
    // Send all stored toasts on initialization
    // const toasts = vizality.api.notifications.getAllToasts();
    // toasts.length && toasts.forEach(toast => _handleToastSend(toast.id));
    vizality.api.notifications.closeAllToasts();
    vizality.api.notifications.on(Events.VIZALITY_TOAST_SEND, _handleToastSend);
    return () => {
      vizality.api.notifications.off(Events.VIZALITY_TOAST_SEND, _handleToastSend);
    };
  }, []);

  return (
    <ToastContainer
      className='vz-toast-container'
      closeOnClick={false}
      draggable={false}
      hideProgressBar={true}
      newestOnTop={true}
      limit={3}
      pauseOnFocusLoss={true}
      // autoClose={false}
      // closeButton={CloseButton}
      pauseOnHover={true}
      position='bottom-right'
    />
  );
});
