const { getModule } = require('../webpack');

const i18n = {
  ...getModule('Messages', 'getLanguages')
};

module.exports = {
  ...i18n
};
