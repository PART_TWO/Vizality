/**
 * Contains all of the function/data that may be useful to allow
 * users and developers to interface more easily with Discord.
 * @module Discord
 * @namespace Discord
 */

import * as snowflake from './Snowflake';
import * as util from './Util';
import * as user from './User';

export {
  snowflake,
  util,
  user
};

export default this;
