/**
 * A collection of various constants, webpack modules, and util that don't quite fit
 * in other submodules or places, but still may be useful.
 * @module Util
 * @memberof Discord
 * @namespace Discord.Util
 */

import { getModule } from '@vizality/webpack';

/**
 * A large collection with a wide range of Discord's utility constants that are used
 * throughout the Discord app.
 */
export const Constants = {
  ...getModule('Permissions', 'ActivityTypes', 'StatusTypes'),
  DEFAULT_AVATARS: {
    ...getModule('DEFAULT_AVATARS')?.DEFAULT_AVATARS
  },
  DEFAULT_GROUP_DM_AVATARS: {
    ...getModule('DEFAULT_GROUP_DM_AVATARS')?.DEFAULT_GROUP_DM_AVATARS
  },
  EMOJI_CATEGORIES: {
    ...getModule('getGuildEmoji')?.categories
  }
};

/**
 * Discord's routes object.
 */
export const Routes = this.Constants?.Routes;

/**
 * Discord's permissions object.
 */
export const Permissions = this.Constants?.Permissions;

/**
 * Discord's routing modules, containing navigation routing functions.
 */
export const router = {
  ...getModule('transitionTo', 'replaceWith', 'getHistory'),
  ...getModule('listeners', 'flushRoute')
};
