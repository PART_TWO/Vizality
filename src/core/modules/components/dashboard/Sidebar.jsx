/**
 * Dashboard sidebar.
 * @component
 */

import { AdvancedScrollerThin, ErrorBoundary, Icon } from '@vizality/components';
import { joinClassNames } from '@vizality/util/dom';
import React, { memo } from 'react';

import { SidebarHeader, SidebarItem, SidebarSeparator } from '.';

export default memo(({ header, className, children }) => {
  const [ collapsed, setCollapsed ] = vizality.api.settings.useSetting('dashboardSidebarCollapse', false);

  return (
    <ErrorBoundary>
      <AdvancedScrollerThin className={joinClassNames('vz-dashboard-sidebar-inner', className)}>
        {!header
          /**
           * If no header prop is provided, render the default Vizality dashboard sidebar header.
           */
          ? (
            <SidebarHeader text='Dashboard'>
              <Icon
                name='Download'
                iconClassName='vz-dashboard-sidebar-collapser-icon'
                className='vz-dashboard-sidebar-collapser'
                onClick={() => setCollapsed(!collapsed)}
                tooltip={collapsed ? 'Expand' : 'Collapsed'}
                tooltipPosition='right'
              />
            </SidebarHeader>
          )
          /**
           * If header prop provided is a string, use that.
           */
          : typeof header === 'string'
            ? <SidebarHeader text={header} />
            /**
             * If header prop is provided but isn't a string, we'll assume it's a React component
             * and render that.
             */
            : header
        }
        {children
          ? children
          : (
            <>
              <SidebarItem icon='Home' text='Home' route='/vizality/dashboard' tooltip={collapsed && 'Home'} />
              <SidebarItem icon='Gear' text='Settings' route='/vizality/settings' tooltip={collapsed && 'Settings'} />
              <SidebarItem icon='Plugin' text='Plugins' route='/vizality/plugins' tooltip={collapsed && 'Plugins'} />
              <SidebarItem icon='Theme' text='Themes' route='/vizality/themes' tooltip={collapsed && 'Themes'} />
              <SidebarItem icon='Scissors' text='Snippets' route='/vizality/snippets' tooltip={collapsed && 'Snippets'} disabled />
              {vizality.manager.builtins.isEnabled('quick-code') && (
                <SidebarItem icon='Compose' text='Quick Code' route='/vizality/quick-code' tooltip={collapsed && 'Quick Code'} />
              )}
              {/* @todo Addon Guidelines, Publish an Addon, Get Verified, Documentation, VSCode Snippets, Manifest Generator */}
              <SidebarSeparator />
              <SidebarItem icon='UnknownUser' text='Development' route='/vizality/development' tooltip={collapsed && 'Development'} disabled />
              <SidebarSeparator />
              <SidebarItem icon='CloudDownload' text='Updater' route='/vizality/updater' tooltip={collapsed && 'Updater'} />
              <SidebarItem
                icon='ClockReverse'
                text='Changelog'
                route='/vizality/changelog'
                tooltip={collapsed && 'Changelog'}
              />
            </>
          )}
      </AdvancedScrollerThin>
    </ErrorBoundary>
  );
});
