/**
 * 
 * @component
 */

import { DiscoverySidebarHeader } from '@vizality/components';
import { joinClassNames } from '@vizality/util/dom';
import React, { memo } from 'react';

export default memo(({ text, children, className }) => {
  return (
    <div className={joinClassNames('vz-dashboard-sidebar-header', className)}>
      <DiscoverySidebarHeader text={text} />
      {children}
    </div>
  );
});
