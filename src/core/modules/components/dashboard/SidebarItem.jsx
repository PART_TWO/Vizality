
/**
 * Dashboard sidebar navigation item.
 * @component
 */

import { ContextMenu, Clickable, Icon } from '@vizality/components';
import { getModule, contextMenu } from '@vizality/webpack';
import { joinClassNames } from '@vizality/util/dom';
import React, { memo, useState } from 'react';

const { closeContextMenu, openContextMenu } = contextMenu;

/**
 * Context menu for dashboard sidebar navigation items.
 * @component
 */
const ItemContextMenu = memo(({ path }) => {
  return (
    <ContextMenu.Menu navId='dashboard-item' onClose={closeContextMenu}>
      <ContextMenu.Item
        id='copy-link'
        label='Copy Link'
        action={() => DiscordNative?.clipboard?.copy(`<vizality:/${path.replace('vizality/', '')}>`)}
      />
    </ContextMenu.Menu>
  );
});

export default memo(({ icon, text, route, selected, disabled, onContextMenu, tooltip }) => {
  const [ isSelected ] = useState(typeof selected === 'boolean' ? selected : vizality.api.routes.getLocation()?.pathname?.startsWith(route));
  const { categoryItem, selectedCategoryItem, itemInner } = getModule('categoryItem', 'selectedCategoryItem', 'itemInner');
  const { container, selected: selectedClass, wrappedLayout, layout, avatar, content, nameAndDecorators, name, wrappedName, clickable } = getModule('wrappedLayout', 'wrappedName', 'layout');

  return (
    <Clickable
      onContextMenu={evt => !disabled && (
        onContextMenu || (
          openContextMenu(evt, () => <ItemContextMenu path={route} />)
        )
      )}
      onClick={() => !disabled && vizality.api.routes.navigateTo(route)}
      className={joinClassNames('vz-dashboard-sidebar-item', categoryItem, container, {
        disabled,
        [`${selectedClass} ${selectedCategoryItem}`]: isSelected,
        [clickable]: !isSelected
      })}
    >
      <div className={joinClassNames('vz-dashboard-sidebar-item-inner', itemInner, layout, wrappedLayout)}>
        <Icon
          className={joinClassNames('vz-dashboard-sidebar-item-icon-wrapper', avatar)}
          iconClassName='vz-dashboard-sidebar-item-icon'
          name={icon}
          tooltip={tooltip}
          tooltipPosition='right'
        />
        <div className={content}>
          <div className={nameAndDecorators}>
            <div className={joinClassNames(name, wrappedName)}>
              {text}
            </div>
          </div>
        </div>
      </div>
    </Clickable>
  );
});
