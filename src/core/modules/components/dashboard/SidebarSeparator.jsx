/**
 * Dashboard sidebar navigation item separator.
 * @component
 */

import { joinClassNames } from '@vizality/util/dom';
import React, { memo } from 'react';

export default memo(({ className }) => (
  <div className={joinClassNames('vz-dashboard-sidebar-separator', className)} />
));
