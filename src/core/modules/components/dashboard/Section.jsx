/**
 * 
 * @component
 */

import { joinClassNames } from '@vizality/util/dom';
import { getModule } from '@vizality/webpack';
import React, { memo } from 'react';

const Header = memo(({ heading, subheading }) => {
  const { marginBottom40 } = getModule('marginBottom20');
  const { headerSubtext } = getModule('headerSubtext');
  const { content } = getModule('wrappedLayout');
  const { h1 } = getModule('h1', 'h2', 'h3');
  const { size32 } = getModule('size24');
  const { base } = getModule('base');

  return (
    <div className={joinClassNames('vz-dashboard-section-header-wrapper', marginBottom40)}>
      <h2 className={joinClassNames('vz-dashboard-section-header', size32, base, content)}>
        {heading}
      </h2>
      <h4 className={joinClassNames('vz-dashboard-section-header-subtext', h1, headerSubtext)}>
        {subheading}
      </h4>
    </div>
  );
});

export default memo(({ heading, subheading, className, children }) => {
  return (
    <div className={joinClassNames('vz-dashboard-section', className)}>
      {heading && (
        <Header heading={heading} subheading={subheading} />
      )}
      <div className='vz-dashboard-section-contents'>
        {children}
      </div>
    </div>
  );
});
