/**
 * 
 * @component
 */

import { toTitleCase, toPlural } from '@vizality/util/string';
import { close as closeModal } from '@vizality/modal';
import { Messages } from '@vizality/i18n';
import { AddonInfoMessage } from '.';
import React, { memo } from 'react';
import { Confirm } from '..';

export default memo(({ addon, type }) => {
  return (
    <Confirm
      header={Messages.VIZALITY_ADDON_UNINSTALL.format({ type: toTitleCase(type) })}
      confirmText={Messages.VIZALITY_UNINSTALL}
      cancelText={Messages.CANCEL}
      onCancel={closeModal}
      onConfirm={async () => {
        closeModal();
        await vizality.manager[toPlural(type)]?._uninstall(addon.addonId);
      }}
    >
      <AddonInfoMessage message={Messages.VIZALITY_ADDON_UNINSTALL_SURE.format({ type })} addon={addon} />
    </Confirm>
  );
});
