/**
 * 
 */

import { toPlural } from '@vizality/util/string';
import { contextMenu } from '@vizality/webpack';
import React, { memo, useState } from 'react';
import { Messages } from '@vizality/i18n';

import { Divider, FormTitle, Anchor, Icon, LazyImage, Button, Switch } from '..';
import { AddonContextMenu } from '.';

const { openContextMenu } = contextMenu;

/**
 * 
 */
const PERMISSIONS = {
  keypresses: {
    icon: memo(({ size }) => <Icon name='Keyboard' size={size} />),
    text: () => Messages.VIZALITY_ADDONS_PERMISSIONS_KEYPRESSES
  },
  use_eud: {
    icon: memo(({ size }) => <Icon name='PersonShield' size={size} />),
    text: () => Messages.VIZALITY_ADDONS_PERMISSIONS_USE_EUD
  },
  filesystem: {
    icon: memo(({ size }) => <Icon name='Copy' size={size} />),
    text: () => Messages.VIZALITY_ADDONS_PERMISSIONS_FS
  },
  ext_api: {
    icon: memo(({ size }) => <Icon name='ImportExport' size={size} />),
    text: () => Messages.VIZALITY_ADDONS_PERMISSIONS_API
  }
};

/**
 * 
 * @component
 */
const Permissions = memo(({ permissions }) => {
  const hasPermissions = permissions && permissions.length > 0;
  if (!hasPermissions) {
    return null;
  }
  return (
    <>
      <Divider />
      <div className='vz-addon-card-permissions'>
        <FormTitle>{Messages.PERMISSIONS}</FormTitle>
        {Object.keys(PERMISSIONS).map(perm => permissions.includes(perm) &&
          <div className='vz-addon-card-permission'>
            {React.createElement(PERMISSIONS[perm].icon, { size: 22 })} {PERMISSIONS[perm].text()}
          </div>)}
      </div>
    </>
  );
});

/**
 * 
 * @component
 */
const Author = memo(({ author }) => {
  return (
    <div className='vz-addon-card-author-wrapper'>
      <Anchor
        type='user'
        userId={author.id}
        className='vz-addon-card-author'
      >
        {author.name || author}
      </Anchor>
    </div>
  );
});

/**
 * 
 * @component
 */
const Description = memo(({ description }) => (
  <div className='vz-addon-card-description'>
    {description}
  </div>
));

/**
 * 
 * @component
 */
const Details = memo(() => {
  return (
    <div className='vz-addon-card-details'>
      <div className='vz-addon-card-detail-wrapper'>
        <div className='vz-addon-card-detail-label'>Rating</div>
        <div className='vz-addon-card-detail-value-wrapper'>
          <Icon
            className='vz-addon-card-detail-value-icon-wrapper vz-addon-card-rating-icon-wrapper'
            iconClassName='vz-addon-card-rating-icon'
            name='Star'
            size='14'
          />
          <div className='vz-addon-card-detail-value vz-addon-card-detail-rating-number'>5</div>
        </div>
      </div>
      <div className='vz-addon-card-detail-wrapper'>
        <div className='vz-addon-card-detail-label'>Downloads</div>
        <div className='vz-addon-card-detail-value-wrapper'>
          <Icon
            className='vz-addon-card-detail-value-icon-wrapper vz-addon-card-rating-icon-wrapper'
            iconClassName='vz-addon-card-rating-icon'
            name='Download'
            size='14'
          />
          <div className='vz-addon-card-detail-value vz-addon-card-detail-downloads-count'>123,663</div>
        </div>
      </div>
      <div className='vz-addon-card-detail-wrapper'>
        <div className='vz-addon-card-detail-label'>Last Updated</div>
        <div className='vz-addon-card-detail-value-wrapper'>
          <Icon
            className='vz-addon-card-detail-value-icon-wrapper vz-addon-card-rating-icon-wrapper'
            iconClassName='vz-addon-card-rating-icon'
            name='ClockReverse'
            size='14'
          />
          <div className='vz-addon-card-detail-value vz-addon-card-detail-updated-date'>11/26/2020</div>
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 */
const AddonIcon = ({ icon }) => {
  return (
    <div className='vz-addon-card-icon'>
      <LazyImage
        className='vz-addon-card-icon-image-wrapper'
        imageClassName='vz-addon-card-icon-img'
        src={icon}
      />
    </div>
  );
}

/**
 * 
 * @component
 */
const Footer = memo(({ addon, type, hasSettings, isEnabled, IsInstalled, onToggle }) => {
  return (
    <div className='vz-addon-card-footer-wrapper'>
      <div className='vz-addon-card-footer'>
        <div className='vz-addon-card-footer-section-left'>
          {IsInstalled &&
            <div className='vz-addon-card-uninstall'>
              <Button
                onClick={evt => {
                  evt.stopPropagation();
                  vizality.manager[toPlural(type)].uninstall(addon.addonId);
                }}
                color={Button.Colors.RED}
                look={Button.Looks.FILLED}
                size={Button.Sizes.ICON}
              >
                <Icon name='Trash' tooltip='Uninstall' />
              </Button>
            </div>
          }
          {/* <div className='vz-addon-card-tags'>
            <div className='vz-addon-card-tag'>
              Pie
            </div>
            <div className='vz-addon-card-tag'>
              Lazer Beams
            </div>
            <div className='vz-addon-card-tag'>
              Stuff
            </div>
          </div> */}
        </div>
        <div className='vz-addon-card-footer-section-right'>
          {IsInstalled && hasSettings &&
            <div className='vz-addon-card-settings-button'>
              <Icon
                className='vz-addon-card-settings-button-icon-wrapper'
                iconClassName='vz-addon-card-settings-button-icon'
                name='Gear'
                tooltip='Settings'
                onClick={() => vizality.api.routes.navigateTo(`/vizality/${toPlural(type)}/${addon.addonId}`)}
              />
            </div>
          }
          <div className='vz-addon-card-toggle-wrapper'>
            <Switch
              className='vz-addon-card-toggle'
              value={isEnabled}
              onChange={() => onToggle(!isEnabled)}
            />
          </div>
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 */
const AddonCard = memo(({ addon, type, hasSettings, isEnabled, IsInstalled, onToggle }) => {
  return (
    <div className='vz-addon-card-header-wrapper'>
      {/* {showPreviewImages && <Previews {...props} />} */}
      <div className='vz-addon-card-content-wrapper'>
        <div className='vz-addon-card-content'>
          <div className='vz-addon-card-header'>
            <AddonIcon icon={addon.manifest.icon} />
            <div className='vz-addon-card-metadata'>
              <div className='vz-addon-card-name-version'>
                <div className='vz-addon-card-name'>
                  {addon.manifest.name}
                </div>
                <span className='vz-addon-card-version'>
                  {addon.manifest.version}
                </span>
              </div>
              <Author author={addon.manifest.author} />
            </div>
          </div>
          <Description description={addon.manifest.description} />
          <Permissions permissions={addon.manifest.permissions} />
          <Footer
            addon={addon}
            type={type}
            hasSettings={hasSettings}
            isEnabled={isEnabled}
            IsInstalled={IsInstalled}
            onToggle={onToggle}
          />
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 */
const AddonCardCompact = memo(({ addon, type, hasSettings, isEnabled, IsInstalled, onToggle }) => {
  return (
    <div className='vz-addon-card-header-wrapper'>
      <div className='vz-addon-card-content-wrapper'>
        <div className='vz-addon-card-content'>
          <div className='vz-addon-card-header'>
            <AddonIcon icon={addon.manifest.icon} />
            <div className='vz-addon-card-metadata'>
              <div className='vz-addon-card-name-version'>
                <div className='vz-addon-card-name'>
                  {addon.manifest.name}
                </div>
                <span className='vz-addon-card-version'>
                  {addon.manifest.version}
                </span>
              </div>
              <Author author={addon.manifest.author} />
            </div>
            {false && <Details />}
            <div className='vz-addon-card-actions'>
              {IsInstalled &&
                <div className='vz-addon-card-uninstall'>
                  <Icon
                    className='vz-addon-card-uninstall-button-wrapper'
                    iconClassName='vz-addon-card-uninstall-button'
                    name='Trash'
                    tooltip='Uninstall'
                    onClick={evt => {
                      evt.stopPropagation();
                      vizality.manager[toPlural(type)].uninstall(addon.addonId);
                    }}
                  />
                </div>
              }
              {hasSettings && (
                <div className='vz-addon-card-settings'>
                  <Icon
                    className='vz-addon-card-settings-button-wrapper'
                    iconClassName='vz-addon-card-settings-button'
                    name='Gear'
                    tooltip='Settings'
                    onClick={() => void 0}
                  />
                </div>
              )}
              <div className='vz-addon-card-toggle-wrapper'>
                <Switch
                  className='vz-addon-card-toggle'
                  value={isEnabled}
                  onChange={() => onToggle(!isEnabled)}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 */
const AddonCardCover = memo(({ addon, type, hasSettings, isEnabled, IsInstalled, onToggle }) => {
  return (
    <div className='vz-addon-card-header-wrapper'>
      <div className='vz-addon-card-content-wrapper'>
        <div className='vz-addon-card-content'>
          <AddonIcon icon={addon.manifest.icon} />
          <div className='vz-addon-card-header'>
            <div className='vz-addon-card-metadata'>
              <div className='vz-addon-card-name-version'>
                <div className='vz-addon-card-name'>
                  {addon.manifest.name}
                </div>
                <span className='vz-addon-card-version'>
                  {addon.manifest.version}
                </span>
              </div>
              <Author author={addon.manifest.author} />
            </div>
          </div>
          <Description description={addon.manifest.description} />
          <Permissions permissions={addon.manifest.permissions} />
          <Footer
            addon={addon}
            type={type}
            hasSettings={hasSettings}
            isEnabled={isEnabled}
            IsInstalled={IsInstalled}
            onToggle={onToggle}
          />
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 * @property {string} addonId afaf
 */
const AddonCardList = memo(({ addon, type, hasSettings, isEnabled, IsInstalled, onToggle, showPreviewImages }) => {
  return (
    <div className='vz-addon-card-header-wrapper'>
      {/* {showPreviewImages && <Previews {...props} />} */}
      {!showPreviewImages && <AddonIcon icon={addon.manifest.icon} />}
      <div className='vz-addon-card-content-wrapper'>
        <div className='vz-addon-card-content'>
          <div className='vz-addon-card-header'>
            {/* {showPreviewImages && <AddonIcon icon={manifest.icon} />} */}
            <div className='vz-addon-card-metadata'>
              <div className='vz-addon-card-name-version'>
                <div className='vz-addon-card-name'>
                  {addon.manifest.name}
                </div>
                <span className='vz-addon-card-version'>
                  {addon.manifest.version}
                </span>
              </div>
              <Author author={addon.manifest.author} />
            </div>
          </div>
          <Description description={addon.manifest.description} />
          <Permissions permissions={addon.manifest.permissions} />
          <Footer
            addon={addon}
            type={type}
            hasSettings={hasSettings}
            isEnabled={isEnabled}
            IsInstalled={IsInstalled}
            onToggle={onToggle}
          />
        </div>
      </div>
    </div>
  );
});

/**
 * 
 * @component
 * @property {string} addonId Addon ID
 * @property {string} type Addon type
 * @property {string} display Addon card display type
 */
export default memo(({ addonId, type, display }) => {
  const addon = vizality.manager[toPlural(type)].get(addonId);
  if (!addon) {
    return;
  }
  const [ isEnabled, setIsEnabled ] = useState(vizality.manager[toPlural(type)].isEnabled(addonId));
  const [ IsInstalled ] = useState(vizality.manager[toPlural(type)].isInstalled(addonId));
  const [ hasSettings ] = useState(vizality.manager[toPlural(type)].hasSettings(addonId));

  const onToggle = async () => {
    if (isEnabled) {
      await vizality.manager[toPlural(type)].disable(addonId);
    } else {
      await vizality.manager[toPlural(type)].enable(addonId);
    }
    setIsEnabled(!isEnabled);
  };
  const handleContextMenu = evt => {
    return openContextMenu(evt, () =>
      <AddonContextMenu
        addonId={addonId}
        type={type}
        hasSettings={hasSettings}
        isEnabled={isEnabled}
        IsInstalled={IsInstalled}
        onToggle={onToggle}
      />
    );
  };

  const getAddonDisplayRender = display => {
    switch (display) {
      case 'compact': return (
        <AddonCardCompact
          addon={addon}
          type={type}
          hasSettings={hasSettings}
          isEnabled={isEnabled}
          IsInstalled={IsInstalled}
          onToggle={onToggle}
        />
      );
      case 'cover': return (
        <AddonCardCover
          addon={addon}
          type={type}
          hasSettings={hasSettings}
          isEnabled={isEnabled}
          IsInstalled={IsInstalled}
          onToggle={onToggle}
        />
      );
      case 'list': return (
        <AddonCardList
          addon={addon}
          type={type}
          hasSettings={hasSettings}
          isEnabled={isEnabled}
          IsInstalled={IsInstalled}
          onToggle={onToggle}
        />
      );
      default: return (
        <AddonCard
          addon={addon}
          type={type}
          hasSettings={hasSettings}
          isEnabled={isEnabled}
          IsInstalled={IsInstalled}
          onToggle={onToggle}
        />
      );
    }
  };

  return (
    <div
      className='vz-addon-card'
      vz-addon-id={addonId}
      onContextMenu={handleContextMenu}
      onClick={evt => {
        // if (evt.target.matches('input') || evt.target.matches('button') || evt.target.matches('svg') || evt.target.matches('a')) {
        //   return;
        // }
        // vizality.api.routes.navigateTo(`/${toPlural(type)}/${addonId}`);
      }}
    >
      {getAddonDisplayRender(display)}
    </div>
  );
});
