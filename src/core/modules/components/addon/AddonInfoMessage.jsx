/**
 * 
 * @component
 */

import { Text, LazyImage } from '..';
import React, { memo } from 'react';

export default memo(({ message, addon, iconSize }) => {
  iconSize = iconSize || '20';
  return (
    <Text>
      <span className='vz-addon-info-message'>
        {message}
      </span>
      <ul className='vz-addon-info-message-ul'>
        <li className='vz-addon-info-message-li' vz-addon-id={addon?.id} key={addon?.id}>
          <div className='vz-addon-info-message-icon'>
            {addon && (
              <LazyImage
                className='vz-addon-info-message-icon-image-wrapper'
                imageClassName='vz-addon-info-message-icon-img'
                src={addon?.manifest?.icon}
                width={iconSize}
                height={iconSize}
              />
            )}
          </div>
          <div className='vz-addon-info-message-name'>
            {addon?.manifest?.name}
          </div>
        </li>
      </ul>
    </Text>
  );
});
