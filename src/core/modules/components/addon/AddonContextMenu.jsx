import React, { memo, useState } from 'react';

import { ContextMenu, LazyImage } from '@vizality/components';
import { contextMenu } from '@vizality/webpack';
import { Messages } from '@vizality/i18n';
import { toPlural } from '@vizality/util/string';

const { closeContextMenu } = contextMenu;

export default memo(({ addonId, type, forceUpdateCard }) => {
  const [ enabled, setEnabled ] = useState(vizality.manager[toPlural(type)].isEnabled(addonId));
  const [ installed, setInstalled ] = useState(vizality.manager[toPlural(type)].isInstalled(addonId));
  return (
    <ContextMenu.Menu navId='vz-addon-context-menu' onClose={closeContextMenu}>
      {enabled
        ? <ContextMenu.Item
          id='disable'
          label='Disable'
          action={async () => {
            await vizality.manager[toPlural(type)].disable(addonId);
            setEnabled(false);
            forceUpdateCard();
          }}
        />
        : <ContextMenu.Item
          id='enable'
          label='Enable'
          action={async () => {
            await vizality.manager[toPlural(type)].enable(addonId);
            setEnabled(true);
            forceUpdateCard();
          }}
        />
      }
      <ContextMenu.Item
        id='settings'
        label='Settings'
        action={() => void 0}
      />
      <ContextMenu.Item
        id='details'
        label='Details'
        action={() => void 0}
      />
      {installed
        ? <ContextMenu.Item
          id='uninstall'
          label='Uninstall'
          color={ContextMenu.Item.Colors.DANGER}
          action={async () => {
            await vizality.manager[toPlural(type)].uninstall(addonId);
            setInstalled(false);
            forceUpdateCard();
          }}
        />
        : <ContextMenu.Item
          id='install'
          label='Install'
          color={ContextMenu.Item.Colors.GREEN}
          action={async () => {
            setInstalled(true);
            forceUpdateCard();
          }}
        />
      }
      <ContextMenu.Separator />
      <ContextMenu.Item
        id='copy-link'
        label='Copy Link'
        action={() => void 0}
      />
      <ContextMenu.Item
        id='copy-id'
        label='Copy ID'
        action={() => void 0}
      />
    </ContextMenu.Menu>
  );
});
